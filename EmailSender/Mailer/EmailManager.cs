﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace EmailSender.Mailer
{
    class EmailManager
    {
        // Check if email has already been sent for a particular day
        public static bool canSendEmail()
        {
            bool canSendEmail = true;
            string today = DateTime.Now.ToString("yyyy-MM-d");
            string lastSent = ConfigurationManager.AppSettings["LastSent"].ToString();

            if(lastSent == today)
            {
                canSendEmail = false;
            }

            return canSendEmail;
        }


        // function to sendEmail given an EmailTemplate Object
        public static bool SendEmail(EmailItem msg)
        {
            var emailFrom = ConfigurationManager.AppSettings["From"].ToString();
            var nameFrom = ConfigurationManager.AppSettings["FromName"].ToString();
            var password = ConfigurationManager.AppSettings["Password"].ToString();
            var smtp = ConfigurationManager.AppSettings["Smtp"].ToString();
            bool sendSucceeded = true;

            try
            {
                System.Net.Configuration.SmtpSection smtpsection = (System.Net.Configuration.SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                var smtpClient = new SmtpClient(smtp);
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new NetworkCredential(emailFrom, password);

                var from = new MailAddress(emailFrom, nameFrom);
                var to = new MailAddress(msg.Email, msg.Name);
                var message = new MailMessage(from, to)
                {
                    Subject = msg.Subject,
                    Body = msg.Body
                };

                smtpClient.Send(message);
                changeAppSettingsConfig("LastSent", DateTime.Now.ToString("yyyy-MM-d"));
                return sendSucceeded;
            }
            catch (Exception ex)
            {
                sendSucceeded = false;
                throw ex;
            }
        }

        private static void changeAppSettingsConfig(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[key].Value = value;
            config.Save(ConfigurationSaveMode.Modified, true);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
