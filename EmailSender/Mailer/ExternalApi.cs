﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace EmailSender.Mailer
{
    class ExternalApi
    {

        // generic function to get application/json data from api
        public static T getData<T>(string url)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync("").Result;

            T data = default(T);
            if (response.IsSuccessStatusCode)
            {
                data = response.Content.ReadAsAsync<T>().Result;
            }

            client.Dispose();
            return data;
        }
    }
}
