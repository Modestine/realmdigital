﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace EmailSender.Mailer
{
    class BirthdayWishes
    {
        public void sendBirthDayWish()
        {
            // Email to send the email, can be changed in App.config
            var sendTo = ConfigurationManager.AppSettings["To"].ToString();
            // check if the email was sent today already
            bool canSend = EmailManager.canSendEmail();

            if (canSend)
            {
                EmailItem message = new EmailItem();
                message.Email = sendTo;
                message.Name = "RealmDigital";
                message.Subject = "BirthDay";
                message.Body = "Happy Birthday ";

                string urlEmployees = "https://interview-assessment-1.realmdigital.co.za/employees";
                string doNotSendUrl = "https://interview-assessment-1.realmdigital.co.za/do-not-send-birthday-wishes";
                List<Employee> employees = ExternalApi.getData<List<Employee>>(urlEmployees);
                List<int> excludeEmployeesIds = ExternalApi.getData<List<int>>(doNotSendUrl);

                var birthdayToDay = 0;
                foreach (Employee employee in employees)
                {
                    // check if the employee has been specifically configured to not receive birthday wishes.
                    if (!excludeEmployeesIds.Contains(employee.id))
                    {
                        var now = DateTime.Now;

                        DateTime dateOfBirth = Convert.ToDateTime(employee.dateOfBirth);
                        DateTime employmentStartDate = Convert.ToDateTime(employee.employmentStartDate);

                        // check if the employee has not started working or he is no longer working
                        if (employmentStartDate < now && (string.IsNullOrEmpty(employee.employmentEndDate) || Convert.ToDateTime(employee.employmentEndDate) > now))
                        {
                            // check if is not a leap year and employee was born on the 29 of Feb, add one day to his birthdayDate.
                            if (!DateTime.IsLeapYear(now.Year) && dateOfBirth.Month == 2 && dateOfBirth.Day == 29)
                            {
                                dateOfBirth.AddDays(1); // or dateOfBirth.AddDays(-1)
                            }

                            // Check if employee birthday is today
                            if (dateOfBirth.ToString("MM-d") == now.ToString("MM-d"))
                            {
                                message.Body = message.Body + " " + employee.name + ",";
                                birthdayToDay++;
                            }
                        }
                    }
                }
                if (birthdayToDay > 0)
                {
                    EmailManager.SendEmail(message);
                }
            }
        }
    }
}
